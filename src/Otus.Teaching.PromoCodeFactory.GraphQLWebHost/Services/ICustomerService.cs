﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.GraphQLWebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.GraphQLWebHost.Services
{
    public interface ICustomerService
    {
        Task<List<CustomerShortResponse>> GetPartnersAsync();

        Task<CustomerResponse> GetParnerById(Guid id);

        Task<Customer> CreateCustomerAsync(CreateOrEditCustomerRequest customerRequest);

        Task<Guid> EditCustomerAsync(Guid id, CreateOrEditCustomerRequest customerRequest);

        Task<Guid> RemoveCustomer(Guid id);
    }
}
