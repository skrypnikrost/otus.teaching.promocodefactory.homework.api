﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.GraphQLWebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.GraphQLWebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.GraphQLWebHost.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomerService(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public async Task<Customer> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(request.PreferenceIds);

            Customer customer = CustomerMapper.MapFromModel(request, preferences);

            await _customerRepository.AddAsync(customer);

            return customer;
        }

        public async Task<Guid> EditCustomerAsync(Guid id, CreateOrEditCustomerRequest customerRequest)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                throw new Exception("Customer wasn't found");

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(customerRequest.PreferenceIds);

            CustomerMapper.MapFromModel(customerRequest, preferences, customer);

            await _customerRepository.UpdateAsync(customer);

            return customer.Id;
        }

        public async Task<CustomerResponse> GetParnerById(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            var response = new CustomerResponse(customer);

            return response;
        }

        public async Task<List<CustomerShortResponse>> GetPartnersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            return response;
        }

        public async Task<Guid> RemoveCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                 throw new Exception("Customer wasn't found");

            await _customerRepository.DeleteAsync(customer);

            return customer.Id;
        }
    }
}
