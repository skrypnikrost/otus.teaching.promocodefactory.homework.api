﻿using HotChocolate;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.GraphQLWebHost.Models;
using Otus.Teaching.PromoCodeFactory.GraphQLWebHost.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.GraphQLWebHost.Types
{
    public class CustomerMutation
    {
        private readonly ICustomerService _customerService;

        public CustomerMutation(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        public async Task<Customer> CreateCustomer( CreateOrEditCustomerRequest customerRequest)
        {
            return await _customerService.CreateCustomerAsync(customerRequest);
        }

        public async Task<Guid> EditCustomer( Guid id, CreateOrEditCustomerRequest customerRequest)
        {
          return await _customerService.EditCustomerAsync(id, customerRequest);
        }

        public async Task<Guid> DeleteCustomer(Guid id)
        {
            return await _customerService.RemoveCustomer(id);
        }


    }
}
