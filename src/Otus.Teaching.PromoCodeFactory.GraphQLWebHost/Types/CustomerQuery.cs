﻿using HotChocolate;
using HotChocolate.Data;
using HotChocolate.Resolvers;
using HotChocolate.Types;
using HotChocolate.Types.Relay;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.GraphQLWebHost.Models;
using Otus.Teaching.PromoCodeFactory.GraphQLWebHost.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.GraphQLWebHost.Types
{
    public class CustomerQuery
    {
        private readonly ICustomerService _customerService;
        private readonly IRepository<Customer> _customerRepository;
        public CustomerQuery(ICustomerService customerService, IRepository<Customer> customerRepository)
        {
            _customerService = customerService;
            _customerRepository = customerRepository;
        }

        [UsePaging]
        [UseFiltering]
        [UseSorting]
        public async Task<List<CustomerShortResponse>> GetCustomers()
        {
            return await _customerService.GetPartnersAsync();
        }

        [UseFiltering]
        [UseSorting]
        public async Task<CustomerResponse> GetCustomer(Guid id)
        {
            return await _customerService.GetParnerById(id);
        }
    }
}
