﻿using Grpc.Core;
using Grpc.Net.Client;
using GrpcServer;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.GrpcClient
{
    class Program
    {
        static void Main(string[] args)
        {
            var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var customersClient = new Customers.CustomersClient(channel);

            Console.WriteLine("Get all customers");

            var customers = customersClient.GetCustomersAsync(new Google.Protobuf.WellKnownTypes.Empty());

            foreach (var cust in customers.Customers)
            {
                Console.WriteLine($"CustomerId: {cust.Id}, FirstName: {cust.FirstName}, LastName: {cust.LastName}");
            }

            Console.WriteLine("Create customer");

            var createCustomerRequest = new CreateCustomerRequest
            {
                CreateRequest = new CreateOrEditCustomerRequest
                {
                   FirstName = "New Name",
                   LastName = "New LastName",
                   Email = "New email"
                }
            };
            createCustomerRequest.CreateRequest.PreferenceIds.Add("c4bda62e-fc74-4256-a956-4760b3858cbd");

            var newCustomerId = customersClient.CreateCustomerAsync(createCustomerRequest);

            Console.WriteLine($"New customer was created with Id : {newCustomerId}");

            Console.WriteLine("Get customer by id");

            var customerById = customersClient.GetCustomerAsync(newCustomerId);

            Console.WriteLine($"CustomerId: {customerById.Id}, FirstName: {customerById.FirstName}, LastName: {customerById.LastName}");

            Console.WriteLine("Edit customer");

            var editCustomerRequest = new EditCustomerRequest
            {
                EditRequest = new CreateOrEditCustomerRequest
                {
                    Id = newCustomerId.CustomerId,
                    FirstName = "Edited Name",
                    LastName = "Edited LastName",
                    Email = "Edited email"
                }
            };

            createCustomerRequest.CreateRequest.PreferenceIds.Add("76324c47-68d2-472d-abb8-33cfa8cc0c84");

            customersClient.EditCustomerAsync(editCustomerRequest);

            Console.WriteLine("Delete Customer");

            customersClient.RemoveCustomerAsync(newCustomerId);
        }
    }
}
