﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromocodeFactory.SignalRServer.Hubs
{
    public interface ICustomerHubClient
    {
        Task CustomerUpdatedMessage(string message);
    }
}
