﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromocodeFactory.SignalRServer.Mappers;
using Otus.Teaching.PromocodeFactory.SignalRServer.Models;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromocodeFactory.SignalRServer.Hubs
{
    public class CustomersHub : Hub<ICustomerHubClient>
    {
        private readonly ILogger<CustomersHub> _logger;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersHub(ILogger<CustomersHub> logger, IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            _logger = logger;
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public async Task<List<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            return response;
        }

        public async Task<CustomerResponse> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            var response = new CustomerResponse(customer);

            return response;
        }

        public async Task<CustomerResponse> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var preferences = await _preferenceRepository
                  .GetRangeByIdsAsync(request.PreferenceIds);

            Customer customer = CustomerMapper.MapFromModel(request, preferences);

            await _customerRepository.AddAsync(customer);

            await Clients.Caller.CustomerUpdatedMessage($"Customer was added with Id: {customer.Id}");

            return new CustomerResponse(customer);
        }

        public async Task<Guid> EditCustomerAsync(Guid id, CreateOrEditCustomerRequest customerRequest)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                throw new Exception("Customer wasn't found");

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(customerRequest.PreferenceIds);

            CustomerMapper.MapFromModel(customerRequest, preferences, customer);

            await _customerRepository.UpdateAsync(customer);

            await Clients.Caller.CustomerUpdatedMessage($"Customer was updated with Id: {customer.Id}");

            return customer.Id;
        }

        public async Task<Guid> RemoveCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                throw new Exception("Customer wasn't found");

            await _customerRepository.DeleteAsync(customer);

            await Clients.Caller.CustomerUpdatedMessage($"Customer was removed with Id: {customer.Id}");

            return customer.Id;
        }

    }
}
