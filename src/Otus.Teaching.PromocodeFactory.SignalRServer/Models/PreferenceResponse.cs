﻿using System;

namespace Otus.Teaching.PromocodeFactory.SignalRServer.Models
{
    public class PreferenceResponse
    {
        public Guid Id { get; set; }
        
        public string Name { get; set; }
    }
}