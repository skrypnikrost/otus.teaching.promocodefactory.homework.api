﻿using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using GrpcServer;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.GrpcServer.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.GrpcServer.Services
{
    public class CustomersService : Customers.CustomersBase
    {
        private readonly ILogger<CustomersService> _logger;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        public CustomersService(ILogger<CustomersService> logger, IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            _logger = logger;
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }


        public async override Task<CustomersList> GetCustomersAsync(Empty request, ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();

            var customerList = new CustomersList();

            var response = customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id.ToString(),
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            customerList.Customers.AddRange(response);

            return customerList;
        }

        public override async Task<CustomerResponse> GetCustomerAsync(CustomerLookupModel request, ServerCallContext context)
        {
            if (request is null)
            {
                throw new RpcException(Status.DefaultCancelled, $"Request is empty");
            }

            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.CustomerId));

            if (customer is null)
            {
                throw new RpcException(Status.DefaultCancelled, $"Customer with {request.CustomerId} doen't exist ");
            }

            var response = new CustomerResponse
            {
                Id = customer.Id.ToString(),
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email
            };

            response.Preferences.AddRange(customer.Preferences.Select(x => new PreferenceResponse
            {
                Id = x.Preference.Id.ToString(),
                Name = x.Preference.Name
            }
            ));

            return response;
        }


        public async override Task<CustomerLookupModel> CreateCustomerAsync(CreateCustomerRequest request, ServerCallContext context)
        {
            if (request is null)
            {
                throw new RpcException(Status.DefaultCancelled, $"Request is empty");
            }

            var preferences = await _preferenceRepository
               .GetRangeByIdsAsync(request.CreateRequest.PreferenceIds.Select(Guid.Parse).ToList());

            Customer customer = CustomerMapper.MapFromModel(request.CreateRequest, preferences);

            await _customerRepository.AddAsync(customer);

            return new CustomerLookupModel { CustomerId = customer.Id.ToString() };
        }

        public async override Task<Empty> EditCustomerAsync(EditCustomerRequest request, ServerCallContext context)
        {
            if (request is null)
            {
                throw new RpcException(Status.DefaultCancelled, $"Request is empty");
            }

            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.EditRequest.Id));
            if (customer is null)
            {
                throw new RpcException(Status.DefaultCancelled, $"Customer with {request.EditRequest.Id} doen't exist ");
            }

            var preferences = await _preferenceRepository
              .GetRangeByIdsAsync(request.EditRequest.PreferenceIds.Select(Guid.Parse).ToList());

            CustomerMapper.MapFromModel(request.EditRequest, preferences, customer);

            await _customerRepository.UpdateAsync(customer);

            return new Empty();
        }

        public override async Task<Empty> RemoveCustomerAsync(CustomerLookupModel request, ServerCallContext context)
        {
            if (request is null)
            {
                throw new RpcException(Status.DefaultCancelled, $"Request is empty");
            }

            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.CustomerId));
            if (customer is null)
            {
                throw new RpcException(Status.DefaultCancelled, $"Customer with {request.CustomerId} doen't exist ");
            }

            await _customerRepository.DeleteAsync(customer);

            return new Empty();
        }
    }
}
