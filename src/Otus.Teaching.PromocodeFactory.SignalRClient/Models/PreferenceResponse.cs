﻿using System;

namespace Otus.Teaching.PromocodeFactory.SignalRClient.Models
{
    public class PreferenceResponse
    {
        public Guid Id { get; set; }
        
        public string Name { get; set; }
    }
}