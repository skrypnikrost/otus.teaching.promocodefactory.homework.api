﻿using Microsoft.AspNetCore.SignalR.Client;
using Otus.Teaching.PromocodeFactory.SignalRClient.Models;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromocodeFactory.SignalRClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var connection = new HubConnectionBuilder()
              .WithUrl("https://localhost:5001/hubs/customers")
              .WithAutomaticReconnect()
              .Build();

            await connection.StartAsync();

            connection.On("CustomerUpdatedMessage", (string message) =>
            {
                Console.WriteLine(message);
            });

            Console.WriteLine("Get all exsisting customers:");

            var customers = await connection.InvokeAsync<List<CustomerShortResponse>>("GetCustomersAsync");

            foreach (var cust in customers)
            {
                Console.WriteLine($"FirstName:{cust.FirstName}, LastName: {cust.LastName}");
            }

            Console.WriteLine("Get exsisting customer:");

            var customer = await connection.InvokeAsync<CustomerResponse>("GetCustomerAsync", "a6c8c6b1-4349-45b0-ab31-244740aaf0f0");

            Console.WriteLine($"FirstName:{customer.FirstName}, LastName: {customer.LastName}");

            Console.WriteLine("Create new customer:");

            var createRequest = new CreateOrEditCustomerRequest
            {
                FirstName = "New Name",
                LastName = "New LastName",
                Email = "New email",
                PreferenceIds = new List<Guid> { Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd") }
            };

            var newCustomer = await connection.InvokeAsync<CustomerResponse>("CreateCustomerAsync", createRequest);

            Console.WriteLine("Edit customer:");

            var editRequest = new CreateOrEditCustomerRequest
            {
                FirstName = "Edited Name",
                LastName = "Edited LastName",
                Email = "Edited email"
            };

            await connection.InvokeAsync<Guid>("EditCustomerAsync", newCustomer.Id, editRequest);

            Console.WriteLine("Remove customer:");

            await connection.InvokeAsync<Guid>("RemoveCustomer", newCustomer.Id);

            Console.ReadLine();
            await connection.StopAsync();
        }
    }
}
